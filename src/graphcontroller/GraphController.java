package graphcontroller;

import domain.ViewsInteractorImpl;
import domain.model.NodeObj;
import domain.network.Messenger;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class GraphController extends Application {
    @FXML
    TableView<NodeObj> waysTable;
    @FXML
    TableColumn<NodeObj, Integer> nodeNumColumn;
    @FXML
    TableColumn<NodeObj, Integer> weightColumn;
    @FXML
    TextField addNodeTextView;
    @FXML
    Button addButton;
    @FXML
    Label additionalError;
    @FXML
    TextField firstNodeForEdgeRemovingTextView;
    @FXML
    TextField secondNodeForEdgeRemovingTextView;
    @FXML
    Label removalError;
    @FXML
    Button removeButton;
    @FXML
    TextField firstNodeForEdgeChangeTextView;
    @FXML
    TextField secondNodeForEdgeChangeTextView;
    @FXML
    Label changeError;
    @FXML
    Button changeEdgeTypeButton;
    @FXML
    Button sendButton;
    @FXML
    TextField messageSizeTextView;
    @FXML
    TextField sendFromNodeTextView;
    @FXML
    TextField sendToNodeTextView;
    @FXML
    RadioButton datagramModeRadio;
    @FXML
    RadioButton logicalConnectionModeRadio;
    @FXML
    ToggleGroup sendModeToggleGroup;
    @FXML
    Label sendInfoLabel;
    @FXML
    TextField waysFromNodeTextView;
    @FXML
    Button calculateWaysButton;

    private ViewsInteractorImpl mMainInteractor;
    private Scene mScene;
    private ObservableList<NodeObj> waysData = FXCollections.observableArrayList();
    @FXML
    private TableColumn<NodeObj, List<Integer>> nodesColumn;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Settings");
        mScene = new Scene(root, 1000, 500);
        primaryStage.setScene(mScene);
        primaryStage.show();
    }

    @FXML
    private void initialize() {
        mMainInteractor = ViewsInteractorImpl.getInstance();
        mMainInteractor.setGraphController(this);

        nodeNumColumn.setCellValueFactory(new PropertyValueFactory<NodeObj, Integer>("number"));
        weightColumn.setCellValueFactory(new PropertyValueFactory<NodeObj, Integer>("weight"));
        nodesColumn.setCellValueFactory(new PropertyValueFactory<NodeObj, List<Integer>>("ways"));

        sendModeToggleGroup = new ToggleGroup();
        datagramModeRadio.setToggleGroup(sendModeToggleGroup);
        logicalConnectionModeRadio.setToggleGroup(sendModeToggleGroup);
        datagramModeRadio.setUserData(Messenger.MessageType.DATAGRAM);
        logicalConnectionModeRadio.setUserData(Messenger.MessageType.LOGICAL_CONNECTION);
    }


    @FXML
    public void onAddClick() {
        additionalError.setVisible(false);
        String textInField = addNodeTextView.getText();
        List<String> inputData = getDataForAdditionFromRegex(textInField);
        if (inputData.isEmpty()) {
            additionalError.setVisible(true);
            return;
        }

        mMainInteractor.addNode(
                inputData.get(0),
                inputData.size() >= 2 ? inputData.subList(1, inputData.size()) : new ArrayList<>());
    }

    @FXML
    public void onRemoveClick() {
        removalError.setVisible(false);
        String firstNode = firstNodeForEdgeRemovingTextView.getText();
        String secondNode = secondNodeForEdgeRemovingTextView.getText();
        if (firstNode.isEmpty() || secondNode.isEmpty()) {
            removalError.setVisible(true);
            return;
        }
        mMainInteractor.removeEdgeByNodes(firstNode, secondNode);
    }

    @FXML
    public void onChangeEdgeTypeClick() {
        changeError.setVisible(false);
        String firstEdge = firstNodeForEdgeChangeTextView.getText();
        String secondEdge = secondNodeForEdgeChangeTextView.getText();
        if (!firstEdge.isEmpty() && !secondEdge.isEmpty())
            mMainInteractor.changeEdgeType(firstEdge, secondEdge);
        else
            changeError.setVisible(true);
    }

    @FXML
    public void onSendClick() {
        sendInfoLabel.setStyle("-fx-text-fill: black");
        sendInfoLabel.setText("Package size:");
        String fromNode = sendFromNodeTextView.getText();
        String toNode = sendToNodeTextView.getText();
        String messageSize = messageSizeTextView.getText();
        Messenger.MessageType messageType = (Messenger.MessageType) sendModeToggleGroup.getSelectedToggle().getUserData();
        if (fromNode.isEmpty()
                || toNode.isEmpty()
                || messageSize.isEmpty()
                || !fromNode.matches("\\d*")
                || !toNode.matches("\\d*")
                || !messageSize.matches("\\d*")){
            sendInfoLabel.setStyle("-fx-text-fill: red");
            sendInfoLabel.setText("Invalid input.");
        } else {
            mMainInteractor.sendMessage(fromNode, toNode, messageSize, messageType);
        }
    }

    @FXML
    public void onCalculateWaysClick() {
        waysTable.setItems(waysData);
    }

    private List<String> getDataForAdditionFromRegex(String inputData) {
        List<String> result = new ArrayList<>();
        inputData = inputData.trim();
        String label;

        String[] splittedData = inputData.split(":");
        if (splittedData.length > 2) {
            return result;
        }
        label = splittedData[0].trim();
        if (label.matches("\\d+"))
            result.add(label);

        if (splittedData.length == 2) {
            String[] drawEdgesTo = splittedData[1].split(",");
            for (String s : drawEdgesTo) {
                if (label.matches("\\d+"))
                    result.add(s.trim());
            }
        }
        return result;
    }

    public void setPackageSize(int size){
        sendInfoLabel.setText("Package size: " + size + " KB");
    }
}