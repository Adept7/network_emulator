package graph;

import domain.Constants;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.GraphParseException;
import org.graphstream.ui.view.Viewer;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class MyGraphImpl implements MyGraph {

//    private final String defaultGraphPath = "/mnt/5050E45350E440F8/KPIlabs/Network_emulator/src/graph/defaultData.dgs";
    private final String defaultGraphPath = "/mnt/5050E45350E440F8/KPIlabs/Network_emulator/src/graph/test.net";
    private Graph mGraph;
    private Viewer mViewer;
    private Random randomGenerator;

    public MyGraphImpl() {
        mGraph = new SingleGraph("network");
        mViewer = new Viewer(mGraph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
        randomGenerator = new Random();
        init();
    }

    private void init() {
        mGraph.setStrict(false);
        mGraph.setAutoCreate(true);
        initStyle();
    }

    private void initStyle() {
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        mGraph.addAttribute("ui.quality");
        mGraph.addAttribute("ui.antialias");
        setStyle("style.css");
    }

    @Override
    public void initDefaultData() {
        try {
            mGraph.read(defaultGraphPath);
        } catch (IOException | GraphParseException e) {
            e.printStackTrace();
        }
        for (Edge e : mGraph.getEdgeSet()) {
            e.setAttribute("ui.color", (double) (randomGenerator.nextInt(2)));
        }

        Edge firstSatellite = getEdgeByNodes("1", "20");
        Edge secondSatellite = getEdgeByNodes("10", "20");
        if (firstSatellite != null && secondSatellite != null){
            firstSatellite.setAttribute("ui.color", Constants.SATELLITE_COLOR);
            secondSatellite.setAttribute("ui.color", Constants.SATELLITE_COLOR);
        }
    }

    @Override
    public Graph getGraphView() {
        return mGraph;
    }

    @Override
    public void setStyle(String filename) {
        mGraph.addAttribute("ui.stylesheet", "url('./graph/" + filename + "')");
    }

    @Override
    public void setEdgesRandomWeight() {
        int limit = Constants.PossibleEdgeWeights.size();
        for (Edge edge : mGraph.getEdgeSet()) {
            edge.addAttribute("ui.label", Constants.PossibleEdgeWeights.get(randomGenerator.nextInt(limit)));
        }
    }

    @Override
    public Viewer display() {
        return mGraph.display();
    }

    @Override
    public Node addNode(String label) {
        return mGraph.addNode(label);
    }

    @Override
    public Node addNodeWithEdgesfromNodeList(String label, List<Node> nodesToConnect) {
        Edge edge;
        Node node = mGraph.addNode(label);
        node.addAttribute("ui.label", label);
        for (Node n : nodesToConnect) {
            edge = mGraph.addEdge(node.toString() + n.toString(), node.getId(), n.getId());
            edge.getNode1().setAttribute("ui.label", n);
        }
        return node;
    }

    @Override
    public Node addNodeWithEdgesfromStringList(String label, List<String> nodesToConnect) {
        Edge edge;
        Node node = mGraph.addNode(label);
        node.addAttribute("ui.label", label);
        for (String n : nodesToConnect) {
            edge = mGraph.addEdge(node.toString() + n, node.getId(), n);
            edge.getNode1().setAttribute("ui.label", n);
        }
        return node;
    }

    @Override
    public void removeEdge(Node node1, Node node2) {
        mGraph.removeEdge(node1, node2);
    }

    @Override
    public void removeEdge(String firstNodeLabel, String secondNodeLabel) {
        mGraph.removeEdge(firstNodeLabel, secondNodeLabel);
    }

    @Override
    public void setEdgeType(String firstNodeLabel, String secondNodeLabel, int type) {
        Edge edge = getEdgeByNodes(firstNodeLabel, secondNodeLabel);
        edge.setAttribute("ui.color", type);
    }

    @Override
    public void setEdgeType(Node node1, Node node2, int type) {
        Edge edge = getEdgeByNodes(node1, node2);
        edge.setAttribute("ui.color", type);
    }

    @Override
    public double getEdgeType(String firstNodeLabel, String secondNodeLabel) {
        Edge edge = getEdgeByNodes(firstNodeLabel, secondNodeLabel);
        if (edge.getAttribute("ui.color") == null)
            return Constants.HALF_DUPLEX_COLOR;
        return edge.getAttribute("ui.color");
    }

    @Override
    public double getEdgeType(Node node1, Node node2) {
        Edge edge = getEdgeByNodes(node1, node2);
        if (edge.getAttribute("ui.color") == null)
            return Constants.HALF_DUPLEX_COLOR;
        return edge.getAttribute("ui.color");
    }

    @Override
    public Edge getEdgeByNodes(Node node1, Node node2) {
        return getEdgeByNodes(node1.toString(), node2.toString());
    }

    @Override
    public Edge getEdgeByNodes(String firstNodeLabel, String secondNodeLabel) {
        String firstNodeId;
        String secondNodeId;

        for (Edge edge : mGraph.getEdgeSet()) {
            firstNodeId = edge.getNode0().getId();
            secondNodeId = edge.getNode1().getId();
            if ((Objects.equals(firstNodeId, firstNodeLabel) && Objects.equals(secondNodeId, secondNodeLabel)) ||
                    (Objects.equals(secondNodeId, firstNodeLabel) && Objects.equals(firstNodeId, secondNodeLabel))) {
                return edge;
            }
        }
        return null;
    }

    private int getRandomEdgeWeight() {
        List<Integer> weights = Constants.PossibleEdgeWeights;
        long range = (long) weights.size() - (long) 1;
        long fraction = (long) (range * randomGenerator.nextDouble());
        return (int) fraction;
    }
}
