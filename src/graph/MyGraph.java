package graph;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.ui.view.Viewer;

import java.util.List;

public interface MyGraph {
    Graph getGraphView();

    void initDefaultData();

    void setStyle(String filename);

    void setEdgesRandomWeight();

    Viewer display();

    Node addNode(String label);

    Node addNodeWithEdgesfromNodeList(String label, List<Node> nodesToConnect);

    Node addNodeWithEdgesfromStringList(String label, List<String> nodesToConnect);

    void removeEdge(Node node1, Node node2);

    void removeEdge(String firstNodeLabel, String secondNodeLabel);

    void setEdgeType(String firstNodeLabel, String secondNodeLabel, int type);

    void setEdgeType(Node node1, Node node2, int type);

    double getEdgeType(String firstNodeLabel, String secondNodeLabel);

    double getEdgeType(Node node1, Node node2);

    Edge getEdgeByNodes(String firstNodeLabel, String secondNodeLabel);

    Edge getEdgeByNodes(Node node1, Node node2);

}
