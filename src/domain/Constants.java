package domain;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final int WAIT_RESPONSE_TIMEOUT = 50; //ms
    public static final int CONNECTION_LIFETIME = 200; //ms
    public static final int SENT_MESSAGES_BUFFER_SIZE = 100;
    public static List<Integer> PossibleEdgeWeights = Arrays.asList(2, 3, 5, 7, 8, 10, 15, 17, 20, 21, 25, 27);

    public static final double DUPLEX_COLOR = 1.0;
    public static final double SATELLITE_COLOR = 0.5;
    public static final double HALF_DUPLEX_COLOR = 0.0;

    public static final double DUPLEX_EDGE_ERROR_PROBABILITY = 0.001;
    public static final double HALF_DUPLEX_EDGE_ERROR_PROBABILITY = 0.001;
    public static final double SATELITE_EDGE_ERROR_PROBABILITY = 0.01;

    public static final double DUPLEX_EDGE_COST_MULTIPLIER = 0.1;
    public static final double HALF_DUPLEX_EDGE_COST_MULTIPLIER = 0.1;
    public static final double SATELITE_EDGE_COST_MULTIPLIER = 0.2;

    public static final int WIRE_EDGE_MTU = 1500; //Byte
    public static final int WIRELESS_EDGE_MTU = 500; //Byte

    public static double getCostMultiplier(double edgeColor){
        if (edgeColor == DUPLEX_COLOR) {
            return DUPLEX_EDGE_COST_MULTIPLIER;
        } else if (edgeColor == HALF_DUPLEX_COLOR) {
            return HALF_DUPLEX_EDGE_COST_MULTIPLIER;
        } else if (edgeColor == SATELLITE_COLOR) {
            return SATELITE_EDGE_COST_MULTIPLIER;
        }
        return 0;
    }

    public static boolean isGetError(double edgeColor){
        double probability = 1;
        if (edgeColor == DUPLEX_COLOR) {
            probability = DUPLEX_EDGE_ERROR_PROBABILITY;
        } else if (edgeColor == HALF_DUPLEX_COLOR) {
            probability = HALF_DUPLEX_EDGE_ERROR_PROBABILITY;
        } else if (edgeColor == SATELLITE_COLOR) {
            probability = SATELITE_EDGE_ERROR_PROBABILITY;
        }
        return Math.random() < probability;
    }

    public static int getMtu(double edgeColor){
        if (edgeColor == SATELLITE_COLOR) {
            return WIRELESS_EDGE_MTU;
        } else {
            return WIRE_EDGE_MTU;
        }
    }
}
