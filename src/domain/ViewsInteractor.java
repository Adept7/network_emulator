package domain;

import com.sun.istack.internal.Nullable;
import domain.network.Messenger;
import graph.MyGraph;
import graphcontroller.GraphController;

import java.util.List;

public interface ViewsInteractor {

    void setGraph(MyGraph graph);

    void setGraphController(GraphController controller);

    void addNode(String label, @Nullable List<String> drawEdgesTo);

    void removeEdgeByNodes(String firstNode, String secondNode);

    void changeEdgeType(String firstNodeLabel, String secondNodeLabel);

    void sendMessage(String fromNode, String toNode, String messageSize, Messenger.MessageType messageType);

    void setPackageSize(int size);
}
