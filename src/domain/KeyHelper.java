package domain;

public class KeyHelper {
    private static final String keyDelimiter = ":";

    public static String makeKey(int firstKeyItem, int secondKeyItem) {
        return firstKeyItem + keyDelimiter + secondKeyItem;
    }

    public static String[] parseKey(String key){
        return key.split(keyDelimiter);
    }
}
