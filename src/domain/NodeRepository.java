package domain;

import domain.model.NodeObj;

import java.util.HashMap;

public class NodeRepository {
    HashMap<Integer, NodeObj> nodeSet;

    public NodeRepository() {
        nodeSet = new HashMap<>();
    }

    public void addNode(NodeObj node) {
        nodeSet.put(node.getId(), node);
    }

    public NodeObj getNode(String id) {
        return getNode(Integer.parseInt(id));
    }

    public NodeObj getNode(int id) {
        return nodeSet.get(id);
    }

    public void removeNode(String id) {
        removeNode(Integer.parseInt(id));
    }

    public void removeNode(int id) {
        nodeSet.remove(id);
    }

    public void updateNode(String id, NodeObj node) {
        updateNode(Integer.parseInt(id), node);
    }

    public void updateNode(int id, NodeObj node) {
        if (node.getId() != id)
            return;
        nodeSet.replace(id, node);
    }

    public void clearRepository() {
        nodeSet.clear();
    }
}
