package domain;

import domain.network.Messenger;
import domain.network.Network;
import graph.MyGraph;
import graphcontroller.GraphController;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;

import java.util.List;

public class ViewsInteractorImpl implements ViewsInteractor {
    private static ViewsInteractorImpl instance;

    public static ViewsInteractorImpl getInstance() {
        return instance == null ? new ViewsInteractorImpl() : instance;
    }

    private static MyGraph mGraph;
    private static Graph mGraphView;
    private Network mNetwork;
    private GraphController mGraphController;

    public ViewsInteractorImpl() {
        mNetwork = Network.getInstance();
        mNetwork.setViewsInteractor(this);
    }

    @Override
    public void setGraph(MyGraph graph) {
        mGraph = graph;
        mGraphView = graph.getGraphView();
        mNetwork.setGraph(mGraph);
    }

    @Override
    public void setGraphController(GraphController controller) {
        mGraphController = controller;
    }

    @Override
    public void addNode(String label, List<String> drawEdgesTo) {
        if (label.isEmpty()) return;
        mGraph.addNodeWithEdgesfromStringList(label, drawEdgesTo);
    }

    @Override
    public void removeEdgeByNodes(String firstNode, String secondNode) {
        mGraph.removeEdge(firstNode, secondNode);
    }

    @Override
    public void changeEdgeType(String firstNodeLabel, String secondNodeLabel) {
        Edge edge = mGraph.getEdgeByNodes(firstNodeLabel, secondNodeLabel);
        double currentType = edge.getAttribute("ui.color");
        if (currentType == 1)
            edge.setAttribute("ui.color", 0.0);
        else
            edge.setAttribute("ui.color", currentType + 0.5);
    }

    @Override
    public void sendMessage(String fromNode, String toNode, String messageSize, Messenger.MessageType messageType) {
        mNetwork.sendMessage(Integer.parseInt(fromNode), Integer.parseInt(toNode), Integer.parseInt(messageSize), messageType);
    }

    @Override
    public void setPackageSize(int size) {
        mGraphController.setPackageSize(size);
    }
}
