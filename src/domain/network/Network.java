package domain.network;

import domain.NodeRepository;
import domain.ViewsInteractor;
import domain.model.NodeObj;
import graph.MyGraph;
import org.graphstream.graph.Node;

public class Network {
    private static Network instance;

    public static Network getInstance() {
        return instance == null ? new Network() : instance;
    }

    private Messenger mMessenger;
    private NodeRepository mNodeRepository;
    private static MyGraph myGraph;
    private ViewsInteractor mViewsInteractor;

    private Network() {
        mMessenger = new Messenger();
        mNodeRepository = new NodeRepository();
    }

    public void setGraph(MyGraph graph) {
        this.myGraph = graph;
    }

    public void setViewsInteractor(ViewsInteractor interactor) {
        this.mViewsInteractor = interactor;
    }

    public void sendMessage(int fromNode, int toNode, int messageSize, Messenger.MessageType messageType) {
        readNetworkTopology();
        initRoutesTables();
//        NodeObj startNode = mNodeRepository.getNode(fromNode);
//        NodeObj finishNode = mNodeRepository.getNode(toNode);
//        mMessenger.sendMessage(startNode, finishNode, messageSize, messageType);
        dumpRouteTables();
    }

    private void readNetworkTopology() {
        NodeObj tmpNode;
        mNodeRepository.clearRepository();
        for (Node node : myGraph.getGraphView().getNodeSet()) {
            tmpNode = new NodeObj(node, mNodeRepository);
            tmpNode.setCallback(mMessenger);
            mNodeRepository.addNode(tmpNode);
        }
    }

    private void initRoutesTables() {
        readNetworkTopology();
        NodeObj startNode = mNodeRepository.getNode(1);
        startNode.startInitRoutesTables();
    }

    private void dumpRouteTables(){
        NodeObj tmpNode;
        for (int i = 0; i < 40; i++){
            tmpNode = mNodeRepository.getNode(i);
            if (tmpNode != null){
                tmpNode.dumpAllRouteTables();
            }
        }
    }

}
