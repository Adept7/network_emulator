package domain.network;

import com.sun.istack.internal.Nullable;
import domain.model.NodeObj;
import domain.model.packages.AbstractPackage;
import domain.model.packages.InformationalPackage;
import domain.model.packages.ServicePackage;

import java.util.List;

public class Messenger implements NodeObj.Callback {
    public enum MessageType {DATAGRAM, LOGICAL_CONNECTION}

    public static int id = 0;

    public static int makeId() {
        return id++;
    }

    public void sendMessage(NodeObj fromNode, NodeObj toNode, int messageSize, MessageType messageType) {
        System.out.printf("Send message from \'%d\' to \'%d\', size = %d, type = %s\n",
                fromNode.getId(), toNode.getId(), messageSize, messageType.toString());

        switch (messageType) {
            case DATAGRAM:
                sendDatagram(fromNode, toNode, messageSize);
                break;
            case LOGICAL_CONNECTION:
                initLogicalConnection(fromNode, toNode, messageSize);
                break;
            default:
                break;
        }
    }

    private void sendDatagram(NodeObj fromNode, NodeObj toNode, int messageSize) {
        InformationalPackage iPackage = new InformationalPackage(fromNode.getId(), toNode.getId(), messageSize);
        fromNode.sendMessage(iPackage);
    }

    private void initLogicalConnection(NodeObj fromNode, NodeObj toNode, int messageSize) {

    }

    public void sendServicePackage(NodeObj fromNode, NodeObj toNode, ServicePackage.MessageType messageType, @Nullable List<Object> data) {
        ServicePackage sPackage = new ServicePackage(fromNode.getId(), toNode.getId());
        sPackage.setMessageType(messageType);
        if (data != null && !data.isEmpty()) {
            sPackage.setData(data);
        }
        fromNode.sendMessage(sPackage);
    }

    @Override
    public void onSendSuccess(AbstractPackage aPackage) {
        System.out.println("package delivered to " + aPackage.getDestinationNodeId());
    }

    @Override
    public void onSendFail(AbstractPackage aPackage) {

    }

}
