package domain.model;

import domain.KeyHelper;

public class RoutesTableItem {
    private int senderNodeId;
    private int destinationNodeId;
    private int prevNode;
    private int nextNode;
    private int passedNodesCounter;

    public RoutesTableItem(int startNode, int finishNode) {
        this.senderNodeId = startNode;
        this.destinationNodeId = finishNode;
    }

    public int getSenderNodeId() {
        return senderNodeId;
    }

    public void setSenderNodeId(int senderNodeId) {
        this.senderNodeId = senderNodeId;
    }

    public int getDestinationNodeId() {
        return destinationNodeId;
    }

    public void setDestinationNodeId(int destinationNodeId) {
        this.destinationNodeId = destinationNodeId;
    }

    public int getPrevNode() {
        return prevNode;
    }

    public void setPrevNode(int prevNode) {
        this.prevNode = prevNode;
    }

    public int getNextNode() {
        return nextNode;
    }

    public void setNextNode(int nextNode) {
        this.nextNode = nextNode;
    }

    public void setPassedNodesCounter(int passedNodesCounter) {
        this.passedNodesCounter = passedNodesCounter;
    }

    public int getPassedNodesCount() {
        return passedNodesCounter;
    }

    public String makeKey() {
        return KeyHelper.makeKey(senderNodeId, destinationNodeId);
    }

    public void update(RoutesTableItem item) {
        if (senderNodeId == item.getSenderNodeId() || destinationNodeId == item.getDestinationNodeId()) {
            if (item.prevNode != 0) prevNode = item.prevNode;
            if (item.nextNode != 0) nextNode = item.nextNode;
            if (item.passedNodesCounter != 0) passedNodesCounter = item.passedNodesCounter;
        }
    }
}
