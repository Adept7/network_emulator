package domain.model;

import domain.Constants;
import domain.KeyHelper;
import domain.NodeRepository;
import domain.model.packages.AbstractPackage;
import domain.model.packages.InformationalPackage;
import domain.model.packages.ServicePackage;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import java.util.*;

public class NodeObj {
    public interface Callback {
        void onSendSuccess(AbstractPackage aPackage);

        void onSendFail(AbstractPackage aPackage);
    }

    private int id;
    private Node view;
    private Callback mCallback;
    private NodeRepository mNodeRepository;
    private HashMap<String, RoutesTableItem> mRoutesTable;
    private ArrayDeque<Integer> sentPackages;
    private boolean isBusy = false;

    public NodeObj(Node view, NodeRepository nodeRepository) {
        this.view = view;
        id = Integer.parseInt(view.getId());
        mRoutesTable = new HashMap<>();
        this.mNodeRepository = nodeRepository;
        sentPackages = new ArrayDeque<>();
    }

    public void setCallback(Callback mCallback) {
        this.mCallback = mCallback;
    }

    public int getId() {
        return id;
    }

    public Node getView() {
        return view;
    }

    public boolean isBusy() {
        return isBusy;
    }

    public void startInitRoutesTables() {
        ServicePackage sPackage = new ServicePackage(id, id);
        sPackage.setMessageType(ServicePackage.MessageType.TABLE_INITIAL_PACKAGE);
        sPackage.setData(new ArrayList<>());
        onReceiveTableInitialPackage(sPackage, 0);
    }

    public void sendMessage(AbstractPackage aPackage) {
        if (haveDirectRoute(aPackage.getDestinationNodeId())) {
            RoutesTableItem routeItem = getDirectRoute(aPackage.getDestinationNodeId());
            if (aPackage instanceof InformationalPackage) {
                List<AbstractPackage> packagesToSend = splitMessageIntoPackage(aPackage, routeItem.getNextNode());
                for (AbstractPackage pac : packagesToSend) {
                    sendPackage(pac, routeItem.getNextNode());
                }
            } else if (aPackage instanceof ServicePackage) {
                sendPackage(aPackage, routeItem.getNextNode());
            }
        } else {
            avalancheSend(aPackage);
        }
    }

    public void sendPackage(AbstractPackage aPackage, int nextNodeId) {
        Edge edge = view.getEdgeBetween(String.valueOf(nextNodeId));
        if (Constants.isGetError(edge.getAttribute("ui.color"))) {
            mCallback.onSendFail(aPackage);
            return;
        }
        aPackage.incPassedNodesCounter();
        NodeObj nextNode = mNodeRepository.getNode(nextNodeId);
        appendSentPackages(aPackage.getId());
        System.out.println("send from " + id + " id = " + aPackage.getId());
        nextNode.onReceivePackage(aPackage, id);
//        int time = calculateCost(edge, aPackage.getDataSize());
    }

    public void avalancheSend(AbstractPackage aPackage) {
        List<Integer> neighborsId = getNeighborsId();
        List<AbstractPackage> packagesToSend;
        for (int i : neighborsId) {
            if (aPackage instanceof InformationalPackage) {
                packagesToSend = splitMessageIntoPackage(aPackage, i);
                for (AbstractPackage pac : packagesToSend) {
                    sendPackage(pac, i);
                }
            } else if (aPackage instanceof ServicePackage) {
                System.out.println("АВАЛАНЧ ИЗ НОДЫ " + id);
                sendPackage(aPackage, i);
            }
        }
    }

    private void onReceivePackage(AbstractPackage aPackage, int receivedFrom) {
        System.out.println("receive in " + id + " id = " + aPackage.getId());
        tryOptimizeRoute(aPackage, receivedFrom);
        if (sentPackages.contains(aPackage.getId())) {
            System.out.println("drop in " + id + " id = " + aPackage.getId());
            return;
        }
        if (aPackage.getDestinationNodeId() == id) {
            mCallback.onSendSuccess(aPackage);

            if (aPackage instanceof InformationalPackage) {

            } else if (aPackage instanceof ServicePackage) {
                switch (((ServicePackage) aPackage).getMessageType()) {
                    case OK:

                        break;
                    case REJECT:

                        break;
                    case WAITING_PACKAGE:

                        break;
                    case RECEIVED_PACKAGE:

                        break;
                    case CONNECTION_REQUEST:

                        break;
                    case DISCONNECTION_REQUEST:

                        break;
                    case TABLE_INITIAL_PACKAGE:
                        onReceiveTableInitialPackage(aPackage, receivedFrom);
                        break;
                    case UPDATE_ROUTE_TABLE:
                        onReceiveUpdateRouteTablePackage(aPackage);
                        break;
                    default:
                        break;
                }
            }
        } else {
            aPackage.incPassedNodesCounter();
            sendMessage(aPackage);
        }
    }

    private void tryOptimizeRoute(AbstractPackage aPackage, int receivedFrom) {
        String key = KeyHelper.makeKey(aPackage.getSenderNodeId(), aPackage.getDestinationNodeId());
        RoutesTableItem currentItem = mRoutesTable.get(key);
        if (currentItem != null) {
            if (aPackage.getPassedNodesCounter() < currentItem.getPassedNodesCount()) {
                RoutesTableItem newTableItem = new RoutesTableItem(aPackage.getSenderNodeId(), aPackage.getDestinationNodeId());
                newTableItem.setNextNode(id);
                newTableItem.setPassedNodesCounter(aPackage.getPassedNodesCounter() - 1);

                ServicePackage servicePackage = new ServicePackage(id, receivedFrom);
                servicePackage.setMessageType(ServicePackage.MessageType.UPDATE_ROUTE_TABLE);
                servicePackage.setData(Collections.singletonList(newTableItem));

                System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
                sendPackage(servicePackage, receivedFrom);
            }
        }
    }

    public List<Integer> getNeighborsId() {
        List<Integer> result = new ArrayList<>();
        Iterator<Node> i = view.getNeighborNodeIterator();
        while (i.hasNext()) {
            Node node = i.next();
            if (Integer.parseInt(node.getId()) != id)
                result.add(Integer.valueOf(node.getId()));
        }
        return result;
    }

    private boolean haveDirectRoute(int destinationNode) {
        String key = KeyHelper.makeKey(id, destinationNode);
        return mRoutesTable.containsKey(key);
    }

    private RoutesTableItem getDirectRoute(int destinationNode) {
        String key = KeyHelper.makeKey(id, destinationNode);
        return mRoutesTable.get(key);
    }

    private void appendSentPackages(int packageId) {
        if (sentPackages.size() == Constants.SENT_MESSAGES_BUFFER_SIZE) {
            sentPackages.poll();
        }
        sentPackages.add(packageId);
    }

    private List<AbstractPackage> splitMessageIntoPackage(AbstractPackage aPackage, int nextNode) {
        List<AbstractPackage> result = new ArrayList<>();
        Edge edge = view.getEdgeBetween(String.valueOf(nextNode));
        double edgeType = edge.getAttribute("ui.color");
        int mtu = Constants.getMtu(edgeType);
        int messageSize = ((InformationalPackage) aPackage).getDataSize();
        int destinationNode = aPackage.getDestinationNodeId();

        if (mtu >= messageSize)
            return Collections.singletonList(aPackage);

        while (messageSize > mtu) {
            result.add(new InformationalPackage(id, destinationNode, mtu));
            messageSize -= mtu;
        }
        result.add(new InformationalPackage(id, destinationNode, messageSize));
        return result;
    }

    private void onReceiveTableInitialPackage(AbstractPackage aPackage, int receivedFrom) {
        ServicePackage tmpPackage;
        RoutesTableItem tmpRouteItem;
        String key;
        List<Integer> passedNodes = new ArrayList<>();
        for (Object i : ((ServicePackage) aPackage).getData()) {
            passedNodes.add((Integer) i);
        }
        passedNodes.add(id);

        for (int i = passedNodes.size() - 2; i >= 0; i--) {
            tmpRouteItem = new RoutesTableItem(id, passedNodes.get(i));
            tmpRouteItem.setNextNode(passedNodes.get(passedNodes.size() - 2));

            key = KeyHelper.makeKey(id, tmpRouteItem.getDestinationNodeId());
            mRoutesTable.put(key, tmpRouteItem);
        }

        for (int i = passedNodes.size() - 2; i >= 0; i--) {
            tmpRouteItem = new RoutesTableItem(passedNodes.get(i), id);
            tmpRouteItem.setPassedNodesCounter(i);
            tmpRouteItem.setNextNode(passedNodes.get(i + 1));
            tmpRouteItem.setPrevNode(i == 0 ? 0 : passedNodes.get(i - 1));

            tmpPackage = new ServicePackage(id, passedNodes.get(i));
            tmpPackage.setMessageType(ServicePackage.MessageType.UPDATE_ROUTE_TABLE);
            tmpPackage.setData(Collections.singletonList(tmpRouteItem));

            System.out.println("СМС С ЗАДНИМ АПГРЕЙДОМ");

            sendMessage(tmpPackage);
        }

        List<Object> newPassedNodes = ((ServicePackage) aPackage).getData();
        newPassedNodes.add(id);

        List<Integer> neighborsId = getNeighborsId();
        ServicePackage sPackage;
        for (int i : neighborsId) {
            if (i != receivedFrom) {
                System.out.println("ПОИСК СЛЕДУЮЩЕЙ НОДЫ");
                sPackage = new ServicePackage(id, i);
                sPackage.setMessageType(ServicePackage.MessageType.TABLE_INITIAL_PACKAGE);
                sPackage.setData(newPassedNodes);
                sendMessage(sPackage);
            }
        }
    }

    private void onReceiveUpdateRouteTablePackage(AbstractPackage aPackage) {
        RoutesTableItem newRouteItem = (RoutesTableItem) ((ServicePackage) aPackage).getData().get(0);
        String key = KeyHelper.makeKey(newRouteItem.getSenderNodeId(), newRouteItem.getDestinationNodeId());
        RoutesTableItem currentTableItem = mRoutesTable.get(key);
        if (currentTableItem != null) {
            currentTableItem.update(newRouteItem);
        } else {
            mRoutesTable.put(key, newRouteItem);
        }
    }

    public void dumpAllRouteTables() {
        System.out.printf("All route tables in node '%d'\n", id);
        mRoutesTable.forEach((s, tableItem) -> dumpRouteTable(tableItem));
    }

    public void dumpRouteTable(RoutesTableItem item) {
        System.out.printf("{%d %d %d %d %d}\n",
                item.getSenderNodeId(),
                item.getDestinationNodeId(),
                item.getPrevNode(),
                item.getNextNode(),
                item.getPassedNodesCount());
    }

//    private int calculateCost(Edge edge, int dataSize) {
//        int time =
//    }
}
