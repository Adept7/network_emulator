package domain.model;

public class Message {
    public enum TransferMode {
        DATAGRAM,
        WITH_LOGICAL_CONNECTION
    }

    private int id;
    private byte[] data;
    private  double size;
    private double remeiningSize;
    private TransferMode transferMode;

    public Message(int id, byte[] data, TransferMode transferMode) {
        this.id = id;
        this.data = data;
        this.transferMode = transferMode;

        size = data.length;
        remeiningSize = size;
    }

}
