package domain.model.packages;

public class InformationalPackage extends AbstractPackage{
    private int dataSize;

    public InformationalPackage(int senderNodeId, int destinationNodeId) {
        super(senderNodeId, destinationNodeId);
    }

    public InformationalPackage(int senderNodeId, int destinationNodeId, int dataSize) {
        super(senderNodeId, destinationNodeId);
        this.dataSize = dataSize;
    }

    public int getDataSize() {
        return dataSize;
    }

    public void setDataSize(int dataSize) {
        this.dataSize = dataSize;
    }
}
