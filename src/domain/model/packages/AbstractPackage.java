package domain.model.packages;

import domain.network.Messenger;

public abstract class AbstractPackage {
    protected int id;
    protected int senderNodeId;
    protected int destinationNodeId;
    protected int lifetime;
    protected int passedNodesCounter;

    public AbstractPackage(int senderNodeId, int destinationNodeId) {
        this.senderNodeId = senderNodeId;
        this.destinationNodeId = destinationNodeId;
        lifetime = 0;
        id = Messenger.makeId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSenderNodeId() {
        return senderNodeId;
    }

    public void setSenderNodeId(int senderNodeId) {
        this.senderNodeId = senderNodeId;
    }

    public int getDestinationNodeId() {
        return destinationNodeId;
    }

    public void setDestinationNodeId(int destinationNodeId) {
        this.destinationNodeId = destinationNodeId;
    }

    public double getLifetime() {
        return lifetime;
    }

    public void appendLifetime(int time){
        lifetime += time;
    }

    public int getPassedNodesCounter() {
        return passedNodesCounter;
    }

    public void incPassedNodesCounter() {
        passedNodesCounter++;
    }
}
