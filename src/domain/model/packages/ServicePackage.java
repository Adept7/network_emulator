package domain.model.packages;

import java.util.List;

public class ServicePackage extends AbstractPackage {
    public enum MessageType {
        CONNECTION_REQUEST, DISCONNECTION_REQUEST,
        OK, REJECT,
        WAITING_PACKAGE, RECEIVED_PACKAGE,
        TABLE_INITIAL_PACKAGE,
        UPDATE_ROUTE_TABLE
    }

    MessageType messageType;
    List<Object> data;

    public ServicePackage(int senderNodeId, int destinationNodeId) {
        super(senderNodeId, destinationNodeId);
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }
}
