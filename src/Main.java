import domain.ViewsInteractorImpl;
import graph.MyGraph;
import graph.MyGraphImpl;
import graphcontroller.GraphController;

import static javafx.application.Application.launch;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        MyGraph graphObj = new MyGraphImpl();
        ViewsInteractorImpl mViewsInteractor = ViewsInteractorImpl.getInstance();

        mViewsInteractor.setGraph(graphObj);
        graphObj.initDefaultData();
        graphObj.setEdgesRandomWeight();
        graphObj.display();

        launch(GraphController.class, args);
    }
}
